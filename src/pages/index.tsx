import DataTable from '../components/dataTable';
import type { NextPage } from 'next';
import { useEffect, useState } from 'react';
import axios from 'axios'
import moment from 'moment'
import { Button } from 'react-bootstrap';

const columns =[
  {
  title: 'Scheme Code',
  dataIndex: 'scheme_code',
  },{
  title: 'Scheme Name',
  dataIndex: 'scheme_name',
  },
  {
  title: 'Nav',
  dataIndex: 'nav',
  },
  {
  title: 'Date',
  dataIndex: 'date',
  render: (date) => moment(date).format('DD/MM/YY'),

  },
  {
  title: 'Return in Last Week',
  dataIndex: 'value',

  },
  ]
const Home: NextPage = () => {
  const [data,setData] =useState([])
  const [currentMethod,setCurrentMethod] =useState(0)

  
  useEffect(()=>{
    const endPoint = currentMethod === 0 ? '/api/top' : '/api/deviation'
    axios.get(endPoint).then(data=>{
      setData(data.data)
    }).catch(error=>{
      console.log(error);
    })
  },[currentMethod])
  
  return (
      <div className="main-container sidebar-closed">
        <title>AMFI</title>
        <div className="row w-100 p-3">
          <div className="col-xl-6 col-md-6 col-sm-6 col-6 d-flex justify-content-center">
              <Button variant={currentMethod === 0 ?"disabled": "primary"}
              onClick={()=>{setCurrentMethod(0)}}>Top 10 Mutual Funds </Button>
          </div>
          <div className="col-xl-6 col-md-6 col-sm-6 col-6 d-flex justify-content-center">
            <Button variant={currentMethod === 1 ?"disabled": "primary"}
              onClick={()=>{setCurrentMethod(1)}}>Standard Deviation </Button>
          </div>
          
        </div>

        <DataTable columns={columns} data={data}/>
      </div>
  );
};

export default Home;
