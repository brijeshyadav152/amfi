import HeaderPage from '../../components/header';
import type { NextPage } from 'next';
import DataTable from '../../components/dataTable';


const Home: NextPage = () => {
  return (
    <>
      {/* <!--  BEGIN NAVBAR  --> */}
      <div className="alt-menu sidebar-noneoverflow">
        <div className="header-container fixed-top">
          <HeaderPage />
        </div>
        {/* <!--  END NAVBAR  --> */}

        {/* <!--  BEGIN MAIN CONTAINER  --> */}
        <div className="main-container sidebar-closed sbar-open" id="container">
          <SideBar />
          {/* <!--  BEGIN CONTENT AREA  --> */}
          <div id="content" className="main-content">
            <div className="layout-px-spacing">
              <div className="row layout-top-spacing">
                <DataTable />
              </div>
            </div>
          </div>
          {/* <!--  END CONTENT AREA  --> */}

        </div>
        {/* <!-- END MAIN CONTAINER --> */}
      </div>
    </>
  );
};

export default Home;
