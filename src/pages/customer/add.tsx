import HeaderPage from '../../components/header';
import type { NextPage } from 'next';
import { Row, Form, Col, Button } from 'react-bootstrap';
import Input from '../../components/formInput';


const AddPage: NextPage = () => {
  return (
    <>
      {/* <!--  BEGIN NAVBAR  --> */}
      <div className="alt-menu sidebar-noneoverflow">
        <div className="header-container fixed-top">
          <HeaderPage />
        </div>
        {/* <!--  END NAVBAR  --> */}
        {/* <!--  BEGIN MAIN CONTAINER  --> */}
        <div className="main-container sidebar-closed sbar-open" id="container">
          <SideBar />
          {/* <!--  BEGIN CONTENT AREA  --> */}
          <div id="content" className="main-content">
            <div className="container">
              <div className="row">
                <div id="flStackForm" className="col-lg-12 layout-spacing layout-top-spacing">
                  <div className="statbox widget box box-shadow">
                    <div className="widget-header">                                
                        <div className="row">
                            <div className="col-xl-12 col-md-12 col-sm-12 col-12">
                                <h4>Stack Forms</h4>
                            </div>                                                                        
                        </div>
                    </div>
                    <div className="widget-content widget-content-area">
                    <Form>
                {/* <Input name={'test'} /> */}
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Email</Form.Label>
                    <Input Placeholder={'Enter email'} Type={'email'} Name={'email'} />
                  </Form.Group>
                  <Form.Group as={Col} controlId="formGridPassword">
                    <Form.Label>Password</Form.Label>
                    <Input Placeholder={'Password'} Type={'password'} Name={'password'} />
                  </Form.Group>
                </Row>

                <Form.Group className="mb-3" controlId="formGridAddress1">
                  <Form.Label>Address</Form.Label>
                  <Input Placeholder={'1234 Main St'} Type={'text'} Name={'address'} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formGridAddress2">
                  <Form.Label>Address 2</Form.Label>
                  <Input Placeholder={'Apartment, studio, or floor'} Type={'text'} Name={'address2'} />
                </Form.Group>

                <Row className="mb-3">
                  <Form.Group as={Col} controlId="formGridCity">
                    <Form.Label>City</Form.Label>
                    <Input Type={'text'} Name={'city'} />
                    {/* <Form.Control /> */}
                  </Form.Group>

                  {/* <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>State</Form.Label>
                    <Form.Select defaultValue="Choose...">
                      <option>Choose...</option>
                      <option>...</option>
                    </Form.Select>
                  </Form.Group> */}

                  <Form.Group as={Col} controlId="formGridZip">
                    <Form.Label>Zip</Form.Label>
                    <Input Type={'text'} Name={'zipCode'} />
                    {/* <Form.Control /> */}
                  </Form.Group>
                </Row>

                <Form.Group className="mb-3" id="formGridCheckbox">
                  <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>

                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!--  END CONTENT AREA  --> */}
        </div>
        {/* <!-- END MAIN CONTAINER --> */}
      </div>
    </>
  );
};

export default AddPage;
