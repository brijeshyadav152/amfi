import React from 'react';
import type { NextPage } from 'next';
import 'antd/dist/antd.css';
import { Table } from 'antd';

const DataTable: NextPage = ({data,columns}) => {
  return (
      <div className="widget-content widget-content-area p-3">
        <div className="table-responsive">
          <Table
            className="table table-hover table-striped"
            bordered
            columns={columns}
            dataSource={data}
          />
         </div>
      </div>
  );
};

export default DataTable;
