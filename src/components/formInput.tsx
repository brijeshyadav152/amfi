import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
// import styles from '../styles/Home.module.css';
import { Form } from 'react-bootstrap';

const Input: NextPage = ({id, Name, Type, Placeholder, className, Label}) => {
    console.log(id)
  return (
    <>
        <Form.Control id={id} name={Name} type={Type} className={className} placeholder={Placeholder} lable={Label} />                        
    </>
  );
};

export default Input;
