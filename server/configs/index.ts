import { envConfig } from '@interfaces/env.interface';
import validateEnv from '@utils/validateEnv';
import project from '../../package.json';

import 'dotenv/config';

const env = validateEnv();

const config: envConfig = {
  isProduction: env.isProduction || env.isProd,
  isDev: env.isDev || env.isDevelopment,
  isTest: env.isTest,
  app: {
    name: project.name,
    description: project.description,
    version: project.version,
    port: env.PORT,
  },
  swagger: {
    enabled: env.ENABLE_SWAGGER,
    path: env.SWAGGER_ENDPOINT,
  },
  apiPrefix: env.API_PREFIX,
  cors: {
    enabled: env.CORS_ENABLED,
    credentials: env.CORS_CREDENTIALS,
  },
  logs: {
    dir: env.logDir,
    format: env.LOG_FORMAT,
  },
  express: {
    useMonitor: env.USE_EXPRESS_STATUS_MONITOR,
    monitorPath: env.EXPRESS_STATUS_MONITOR_PATH,
  },
  mongoose: {
    url: env.MONGODB_URL
  }
};

export default config;
