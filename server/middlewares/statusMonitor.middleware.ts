import config from '@configs';
import { logger } from '@utils/logger';
import { Application } from 'express';
import * as expressStatusMonitor from 'express-status-monitor';

class StatusMonitor {
  public mount(_express: Application) {
    logger.info("Booting the 'StatusMonitor' middleware...");

    // Define your status monitor config
    const monitorOptions: object = {
      title: config.app.name,
      path: config.express.monitorPath,
      spans: [
        {
          interval: 1, // Every second
          retention: 60, // Keep 60 data-points in memory
        },
        {
          interval: 5,
          retention: 60,
        },
        {
          interval: 15,
          retention: 60,
        },
      ],
      chartVisibility: {
        mem: true,
        rps: true,
        cpu: true,
        load: true,
        statusCodes: true,
        responseTime: true,
      },
      healthChecks: [
        {
          protocol: 'http',
          host: 'localhost',
          path: '/',
          port: `${config.app.port}`,
        },
        {
          protocol: 'http',
          host: 'localhost',
          path: `/${config.apiPrefix}`,
          port: `${config.app.port}`,
        },
      ],
    };

    logger.info(JSON.stringify(monitorOptions));

    // Loads the express status monitor middleware
    _express.use(expressStatusMonitor.default(monitorOptions));
  }
}

export default new StatusMonitor();
