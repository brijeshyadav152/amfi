import * as Mongoose from "mongoose";
import { model } from "mongoose";  

interface INav {
  scheme_code: string;
  scheme_name: string;
  isin_growth: string;
  nav: Number;
  date: Date;
}

const NavSchema = new Mongoose.Schema({
  scheme_code: String,
  scheme_name: String,
  isin_growth: String,
  nav: Number,
  date: {
      type: Date,
      default: new Date(),
  }
});

export default model<INav>("amfi",NavSchema)
