import NavModel from '@/models/nav.model';
import { Controller, Get } from 'routing-controllers';
import moment from 'moment'

@Controller('/api')
export class IndexController {
  @Get('/')
  index() {
    return 'OK';
  }

  @Get('/top')
  async getTopTen() {
   try{
    const startDate = new Date("2022-04-03T18:30:00.000+00:00").toISOString()
    const endDate = new Date("2022-04-10T18:30:00.000+00:00").toISOString()
    const data = await NavModel.find({date :{$in:[startDate,endDate]}, raw:true},null,{lean:true});
    const schemes = [...new Set(data.map(item => item.scheme_code))]

    const navs= schemes.map(scheme=>{
     const startData=  data.find(dt=> scheme === dt.scheme_code &&  startDate === dt.date.toISOString())
     const endData=  data.find(dt=> scheme === dt.scheme_code && endDate === dt.date.toISOString());
      const navReturn = startData && endData && Math.abs((Number(endData.nav) -Number(startData.nav))/Number(endData.nav));
      return {...endData,value:navReturn}
    })
    const filteredNav = navs.filter(value => Object.keys(value).length > 1);
    const filteredNavs = filteredNav.sort((a, b)=> Number(a.value) - Number(b.value))
    const finalData = filteredNavs.slice(0,10)
    
    return finalData;
  }catch(error){
    return error
  }
  }

  @Get('/deviation')
  async getDevaition() {
    const startDate = new Date("2022-04-03T18:30:00.000+00:00").toISOString()
    const endDate = new Date("2022-04-10T18:30:00.000+00:00").toISOString()
    const data = await NavModel.find({date :{$in:[startDate,endDate]}, raw:true},null,{lean:true});
    const schemes = [...new Set(data.map(item => item.scheme_code))]

    const navs= schemes.map(scheme=>{
     const schemeData = data.filter(dt=> scheme === dt.scheme_code);
     const navValues  = schemeData.map(nav=> nav.nav)
      const deviation = this.getStandardDeviation(navValues);
      return {...schemeData[0],value:deviation}
    })
    const filteredNav = navs.filter(value => Object.keys(value).length > 1);
    const filteredNavs = filteredNav.sort((a, b)=> Number(a.value) - Number(b.value))
    const finalData = filteredNavs.slice(0,10)
    
    return finalData;
  }

   getStandardDeviation (navs) {
    const n = navs.length
    const mean = navs.reduce((a, b) => a + b) / n
    return Math.sqrt(navs.map(x => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n)
  }
}
