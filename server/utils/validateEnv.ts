import { bool, cleanEnv, num, port, str } from 'envalid';

function validateEnv() {
  return cleanEnv(process.env, {
    NODE_ENV: str(),
    PORT: port(),
    API_PREFIX: str(),
    CORS_ENABLED: bool(),
    CORS_CREDENTIALS: bool(),
    logDir: str(),
    LOG_FORMAT: str(),
    USE_EXPRESS_STATUS_MONITOR: bool(),
    EXPRESS_STATUS_MONITOR_PATH: str(),
    ENABLE_SWAGGER: bool(),
    SWAGGER_ENDPOINT: str(),
    MONGODB_URL:str(),
  });
}

export default validateEnv;
