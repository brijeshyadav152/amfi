/** @type {import('next').NextConfig} */

const moduleExports = {
  reactStrictMode: true,
  compress: false,
};

module.exports = moduleExports;
